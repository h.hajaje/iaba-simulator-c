#include <iostream>
using namespace std;
#include <cmath>
#include <ctime>
#include <fstream>
#include <cstring>

//We assume that the queues are always full (saturated mode)
//but we can use a stochastic model to model packet arrivals

int CSMA_MOD;
#undef  DEBUG
//#define DEBUG

#define MAX_DELAY 2048
#define INT64 __int64_t
#define alpha	0.5
#define beta1	0.8
#define beta2	0.9
#define MAX		2000		//MAX number of nodes in the network
#define L 14				//Length of packets in time slots
#define Lack	2
#define CW_TR	2
#define ACK 0
#define QUEUE 0
#if  ACK
#define CW_ACK 1		//CW for ack frames
#define ACK_TIMEOUT (Lack+CW_ACK) //Time needed to detect a collision
#else
#define CW_ACK 0		//CW for ack frames
#define ACK_TIMEOUT 0 //Time needed to detect a collision
#endif
#define MacMinBE	3	//Min Backoff Exponent
#define aMaxBE		11	//Maximum Backoff Exponent
#define macMaxCSMABackoffs	7 //Maximum of Backoffs before frame transmkission fails
#define macMaxFrameRetries	3
#define CCA_Duration	2	//How many slots CCA is performed
#define SIM_TIME		1000000 //0 //The total simulation time

ofstream fileT;
ofstream fileE;
ofstream fileEC;
ofstream fileR;
ofstream fileF;
ofstream fileS;
ofstream fileC;
ofstream fileI;
ofstream fileA;
ofstream fileD;
ofstream fileDL;
ofstream filePC;

INT64	tick;
typedef struct
{
    INT64 start; //start time on which the medium is busy
    int	  lengthFrame;
    bool  collision; //Current Status of the medium
    bool  idle;
    bool  sending;
    int	  timeCollission;
    int   timeSending;
    int   timeIdle;
} medium;

typedef struct
{
    int		id;			//The id of the node
    INT64	ST;			//The time when the frame starts being transmitted
    int		NB;			//Number of backoffs
    int		CW;			//Number of CCA
    int		BE;			//Backoff exponent
    int		frameLength; //Length of the frame to be transmitted
    int		BE_KEB;		//KEB
    int		backoffPeriod;				//Random delay before frame can be transmitted
    int		backoffPeriod_backup;		//Backup when  ACK has to be transmitted
    int		receiver;	//Destination station
    int		retries;
    int		remainingDelayIBEB;
    double  pc;			//Probability of collision
    double  h;
    int		time_generated;
} frame;


char *my_itoa(int num, char *str)
{
    if(str == NULL)
    {
        return NULL;
    }
    sprintf(str, "%d", num);
    return str;
}

int getBackoffPeriod(frame &f,double nodeStats[][MAX],int Wopt)
{
    int delay;
    double h,pc;
    switch(CSMA_MOD)
    {
    case 0: //Original BE
        return (rand()%(int)(pow(2.0,f.BE)));
    case 1: //ABA Improved
        pc=(1.0*nodeStats[1][f.id])/(nodeStats[0][f.id]+nodeStats[1][f.id]); //probability of collision
        pc=beta1*f.pc+(1-beta1)*pc; //Exponential Average
        f.pc=pc;
        delay=(int)ceil(pc*MAX_DELAY);
        return rand()%(1+delay);
    case 2: //W optimal Improved
        return rand()%Wopt;
    case 3: //IABA Improved
        h=(nodeStats[0][f.id]+nodeStats[1][f.id]+nodeStats[3][f.id])/(1.0+tick);
        h=-0.003+0.004/(h+0.001);
        h=0.8*h+((0.2*(nodeStats[1][f.id]))/(1.0+nodeStats[0][f.id]+nodeStats[1][f.id]));
        h=beta2*f.h+(1-beta2)*h; //Exponential Average
        f.h=h;
        delay=(int)(h*MAX_DELAY);
        return rand()%delay;
    }
    return 0;
}
void generateACK(frame frameTable[],int node)
{
    int receiver=frameTable[node].receiver;
    frameTable[receiver].ST=-1;
    frameTable[receiver].frameLength=Lack;
    frameTable[receiver].CW=CW_ACK;
    frameTable[receiver].NB=0;
    frameTable[receiver].BE=MacMinBE;
    frameTable[receiver].backoffPeriod_backup=frameTable[receiver].backoffPeriod;
    frameTable[receiver].backoffPeriod=0; //Since it's an ACK
    frameTable[receiver].receiver=node;
}

void generateFrame(frame frameTable[],int node,int delay,int NUM_NODES,int Wopt,double nodeStats[][MAX],int retries=0)
{
    frameTable[node].ST=-1;
    frameTable[node].frameLength=L;
    frameTable[node].CW=CW_TR;
    frameTable[node].NB=0;
    frameTable[node].retries=retries;
    frameTable[node].BE=MacMinBE;
    frameTable[node].backoffPeriod=delay+getBackoffPeriod(frameTable[node],nodeStats,Wopt); //Since completed transmission is successful after receiving ACK
    do
    {
        frameTable[node].receiver=rand()%NUM_NODES;
    }
    while(frameTable[node].receiver==node);
    frameTable[node].time_generated=tick;
}


int runSimulator(int NUM_NODES,int Wopt)
{
    int success=0,collision=0;
    int	totalCollided=0,nbCollisions=0;
    int nbCollisionDiscarded=0,nbBackoffDiscarded=0;
    int totalCollidedRatio[MAX]= {0};
    tick=0; //The current time of the simulation
    frame frameTable[MAX];
    frame f;
    medium channel;
    double nodeStats[8][MAX]= {0}; //col 0 Successful Transmission, col 1 collisions, col 2 successful receive, col 3 CCA, col 4 idle, col 5 ACK, col 6 Failures (assuming MaxFrameRetries=1), 7 CCA1
    double delays[MAX]= {0};
    double nb_cycles=0;
    double cycles[MAX][3]= {0}; //0 Success 1 Busy 2 Collision
    //Initialize the frame table
    channel.start=-2*L; //So channel will be idle
    channel.collision=false;
    channel.idle=true;
    channel.sending=false;
    channel.lengthFrame=0;
    channel.timeCollission=0;
    channel.timeIdle=0;
    channel.timeSending=0;
    for(int node=0; node<NUM_NODES; node++)
    {
        //Init stations stat
        nodeStats[0][node]=1;
        nodeStats[1][node]=1;
        //Init frame
        f.id=node;
        f.pc=0;
        f.h=1.0;
        f.BE_KEB=MacMinBE;
        //Insert frame into frameTable
        frameTable[node]=f;
        generateFrame(frameTable,node,0,NUM_NODES,Wopt,nodeStats);
    }
    //Start the simulation
    while(tick<SIM_TIME)
    {
        //Service each node
        //First check who needs to transmit
        for(int node=0; node<NUM_NODES; node++)
        {
            //Check if node is not trasmitting
            if((frameTable[node].ST!=-1) && ((frameTable[node].ST+frameTable[node].frameLength-1)>=tick)) //Node is transmitting Packet
                continue;
            if(frameTable[node].CW==0)
            {
                //Transmit frame
                channel.start=tick;
                channel.sending=true;
                channel.idle=false;
                if(channel.lengthFrame<frameTable[node].frameLength)
                    channel.lengthFrame=frameTable[node].frameLength;
                frameTable[node].ST=tick;
                frameTable[node].CW=2; //To fix bug
                //We check for collisions at the end
            }
        }
        //Second do CCA
        for(int node=0; node<NUM_NODES; node++)
        {
            //Check if node is not trasmitting
            if((frameTable[node].ST!=-1) && (frameTable[node].ST+frameTable[node].frameLength>=tick)) //Node is transmitting Packet
                continue;
            //Check delay counter
            if(frameTable[node].backoffPeriod>0)
            {
                frameTable[node].backoffPeriod--;
                nodeStats[4][node]++; //Idle (should be fixed station might be receiving)
                continue;
            }
            //CCA
            if(frameTable[node].CW>0)
            {
                //Perform Clear Channel Assessment
                nodeStats[3][node]++; //Increment how many times the node made a CCA
                if(frameTable[node].CW==2)
                {
                    nodeStats[7][node]++;
                    nb_cycles++; //counts how many cycles performed by a node
                }
                if(!channel.idle)  //channel is busy
                {
                    frameTable[node].NB++;
                    cycles[node][1]++; //Busy cycle
                    if(frameTable[node].NB<=macMaxCSMABackoffs)
                    {
                        frameTable[node].CW=CW_TR;
                        frameTable[node].BE=min(frameTable[node].BE+1,aMaxBE);
                        frameTable[node].backoffPeriod=getBackoffPeriod(frameTable[node],nodeStats,Wopt);
                    }
                    else
                    {
                        //Start Over (Failure)
                        generateFrame(frameTable,node,0,NUM_NODES,Wopt,nodeStats,frameTable[node].retries);
                        nodeStats[6][node]+=frameTable[node].frameLength; //Failed to deliver frame discarded due to MAX bacoffs
                        nbBackoffDiscarded++;
                        if(frameTable[node].frameLength==Lack)
                        {
                            cout<<"ACK involved in backoff discard!!!!\n";
                            exit(0);
                        }
                        //printStatus(frameTable,channel,success,collision,tick,NUM_NODES,nodeStats);
                        //system("pause");
                    }
                }
                else
                {
                    frameTable[node].CW--;
                }
            }
        }
        //Check if there is a collision
        for(int node1=0; node1<NUM_NODES; node1++)
        {
            if(frameTable[node1].ST==-1) continue; //Node is not transmitting
            for(int node2=node1+1; node2<NUM_NODES; node2++)
            {
                if(frameTable[node1].ST==frameTable[node2].ST)
                {
                    channel.collision=true;
                    channel.sending=false;
                }
            }
        }
        bool completed=false;
        int nodeT[MAX]= {0};
        //Check if a node has finished transmission
        for(int node=0; node<NUM_NODES; node++)
        {
            if((frameTable[node].ST!=-1) && (frameTable[node].ST+frameTable[node].frameLength-1==tick))
            {
                //cout<<"Transmission Completed"<<endl;
                //Transmission completed
                completed=true;
                nodeT[node]=1; //Keep track of the node who completed a successful transmission
            }
        }
        if(channel.collision)
            channel.timeCollission++;
        if(channel.sending)
            channel.timeSending++;
        if(channel.idle)
            channel.timeIdle++;


        if(completed)
        {
            if(channel.collision)
            {
                int check=0;
                collision++;
                for(int node=0; node<NUM_NODES; node++)
                {
                    if(nodeT[node]==0) continue; //Station not involved
                    nodeStats[1][node]+=frameTable[node].frameLength; //Update collision stat for the node
                    if(frameTable[node].frameLength==Lack)
                    {
                        cout<<"ACK involved in collision!!!!\n";
                        exit(0);
                    }
                    cycles[node][2]++; //Collision cycle
                    check++;
                    //Reschedule Frame for Node
                    generateFrame(frameTable,node,0,NUM_NODES,Wopt,nodeStats,frameTable[node].retries);
                    frameTable[node].retries++;
                    if(frameTable[node].retries>macMaxFrameRetries)
                    {
                        delays[node]+=1+tick-frameTable[node].time_generated;
                        frameTable[node].retries=0;
                        nodeStats[6][node]+=frameTable[node].frameLength; //Discarded due to collision
                        nbCollisionDiscarded++;
                    }
                }
                if(check<2)
                {
                    cout<<"Less than two stations were involved in a collision\n";
                    exit(0);
                }
                totalCollidedRatio[check]++;
                totalCollided+=check;
                nbCollisions++;
            }
            else
            {
                //Successful Transmission
                int check=0;
                success++; //Should happen only for one node
                for(int node=0; node<NUM_NODES; node++ )
                {
                    if(nodeT[node]==0) continue;
                    check++;
                    if(frameTable[node].frameLength==Lack)
                        nodeStats[5][node]+=frameTable[node].frameLength;	//Ack
                    else
                    {
                        nodeStats[0][node]+=frameTable[node].frameLength;	//Successful transmission
                        cycles[node][0]++; //Successful cycle
                    }

                    nodeStats[2][frameTable[node].receiver]+=frameTable[node].frameLength;			//Succecssful reception
                    int frameLength=frameTable[node].frameLength;
                    //Generate The ACK if it's not already an ACK
                    if(ACK && frameTable[node].frameLength==L)
                    {
                        generateACK(frameTable, node); //Generate an ack at the receiver side
                    }
                    //If regular packet add delay to receive ACK
                    if(ACK && frameTable[node].frameLength==L)
                        generateFrame(frameTable,node,0,NUM_NODES,Wopt,nodeStats);
                    else
                        generateFrame(frameTable,node,0,NUM_NODES,Wopt,nodeStats); //Should be modified when we use queue (since delay will be increased)
                    //Set the backoff period to it's original value
                    if(frameLength==Lack)
                        frameTable[node].backoffPeriod=frameTable[node].backoffPeriod_backup;
                }
                if(check!=1)
                {
                    cout<<"More than one station transmitted successfully at the same time\n";
                    exit(0);
                }
            }
        }
#ifdef DEBUG
        int timeSending=0;
        for(int node=0; node<NUM_NODES; node++)
            timeSending+=nodeStats[0][node]-1;
        cout<<"Channel Sending Time:"<<channel.timeSending<<endl;
        cout<<"Verification: timeSending="<<timeSending<<endl;
        system("pause");
#endif
        //Update status if completed
        if(completed)
        {
            completed=false;
            channel.idle=true;
            channel.collision=false;
            channel.sending=false;
        }
        tick++; //Increase time by one unit
    }
    //Calculate the fairness index based on the number of successful transmissions using Jain Index
    double sum=0,sumc=0,squareSum=0,fairnessIndex;
    for(int node=0; node<NUM_NODES; node++)
    {
        sum+=nodeStats[0][node];
        sumc+=nodeStats[1][node];
        squareSum+=nodeStats[0][node]*nodeStats[0][node];
    }
    fairnessIndex=(sum*sum)/(NUM_NODES*squareSum);
    //Print it to file
    fileF<<fairnessIndex<<"\t";

    //Calculate the average energy energy
    //For this calculation we assume that a node is either in Tx, Rx or idle
    double Rx=40,Tx=30, idle=0.8,cca=40;
    double energy[MAX]= {0},avgEnergy=0;
    double energyC[MAX]= {0},avgEnergyC=0;
    for(int node=0; node<NUM_NODES; node++)
    {
        //We count energy consumed during simulations
        energy[node]=(nodeStats[0][node]+nodeStats[1][node])*Tx
                     +nodeStats[3][node]*cca+(nodeStats[4][node]-nodeStats[0][node])*idle + nodeStats[0][node]*(Rx);
        avgEnergy+=energy[node];
    }
    avgEnergy/=(1000*NUM_NODES); //Calculate as a function of slot=0.32ms
    //Convert it to second
    avgEnergy*=(0.32/1000); //Now we have a Watt
    //Print Average energy
    fileE<<avgEnergy <<"\t";

    for(int node=0; node<NUM_NODES; node++)
    {
        //We count energy transmissions
        energyC[node]=nodeStats[1][node]*(Tx);
        avgEnergyC+=energyC[node];
    }
    avgEnergyC/=(1000*NUM_NODES); //Calculate as a function of slot=0.32ms
    //Convert it to second
    avgEnergyC*=(0.32/1000); //Now we have a Watt
    //Print Average energy
    fileEC<<avgEnergyC <<"\t";
    //Calculate the reliability. We define reliability as success/(success+collisions)
    double R[MAX]= {0},totalR=0;
    for(int node=0; node<NUM_NODES; node++)
    {
        R[node]=(1.0*nodeStats[0][node])/(nodeStats[0][node]+nodeStats[6][node]);
        totalR+=R[node];
    }
    totalR/=NUM_NODES;
    //Print Reliability
    fileR<<totalR <<"\t";

    //Calculate Throughput
    double throughput=(1.0*channel.timeSending)/SIM_TIME;
    fileT<<throughput<<"\t";

    //Calculate collisions rate
    fileC<<(1.0*channel.timeCollission)/SIM_TIME<<"\t";

    //calculate average delay to send a frame
    double delay=0;
    double n_sc=0; //number successful cycles
    for(int node=0; node<NUM_NODES; node++)
        //delay+=(1.0*delays[node]*L)/(nodeStats[0][node]+nodeStats[0][node]);
        n_sc+=cycles[node][0];

    n_sc/=NUM_NODES;
    delay = SIM_TIME/n_sc;

    fileDL<<delay<<"\t";

    //Calculate Idle rate
    fileI<<(1.0*channel.timeIdle)/SIM_TIME<<"\t";


    //Print to final simulation results
    fileS<<fairnessIndex<<"\t"<<throughput<<"\t"<<avgEnergy<<"\t"<<totalR<<"\t"<<endl;
    int totalData=0,totalACKs=0;
    for(int node=0; node<NUM_NODES; node++)
    {
        totalData+=nodeStats[0][node];
        totalACKs+=nodeStats[5][node];
    }

    //Calculate Data Throughput
    fileD<<(1.0*totalData)/SIM_TIME<<"\t";

    //Calculate ACK throughput

    fileA<<(1.0*totalACKs)/SIM_TIME<<"\t";

    //Calculate Probability of Collision

    double pc=0;
    for(int node=0; node<NUM_NODES; node++)
    {
        pc+=(1.0*nodeStats[1][node])/(nodeStats[1][node]+nodeStats[0][node]);
    }
    pc/=NUM_NODES;
    filePC<<pc<<"\t";

    return 0;
}
int main()
{
    cout.setf(ios::fixed,ios::floatfield);
    cout.precision(5);
    int nodesT[26]= {5,10,15,20,25,30,35,40,45,50,55,60,65,70,80,90,100,120,140,160,180,200, 240, 290, 340, 400};
    int nodesWopt[26]= {26, 56, 85, 115, 144, 179, 210, 249, 263, 306, 324, 363, 388, 418, 492, 563, 598, 729, 835, 957, 1083, 1214, 1443, 1745, 2046, 2048};
    srand(time(0));
    char fileName[80];
    char prefix[80];
    char test[80];
    if(ACK)
        strcpy(prefix,"ACK_");
    else
        strcpy(prefix,"NOACK_");
    if(QUEUE)
        strcat(prefix,"QUEUE_");
    else
        strcat(prefix,"SATUR_");
    strcat(prefix,my_itoa(L,test));
    strcat(prefix,"_");
    strcat(prefix,my_itoa(macMaxCSMABackoffs,test));
    strcat(prefix,"_");
    strcat(prefix,my_itoa(macMaxFrameRetries,test));
    strcat(prefix,"_");

    strcpy(fileName,prefix);
    strcat(fileName,"Throughput.txt");
    fileT.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Energy.txt");
    fileE.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"EnergyCollisions.txt");
    fileEC.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Reliability.txt");
    fileR.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Fairness.txt");
    fileF.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"SimulationResults.txt");
    fileS.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Collisions.txt");
    fileC.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Idle.txt");
    fileI.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"ACK.txt");
    fileA.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Data.txt");
    fileD.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"Delay.txt");
    fileDL.open(fileName);

    strcpy(fileName,prefix);
    strcat(fileName,"ProbabilityCollision.txt");
    filePC.open(fileName);
    fileT<<"\tBEB\tABA\tWopt\tIABA\n";
    fileE<<"\tBEB\tABA\tWopt\tIABA\n";
    fileEC<<"\tBEB\tABA\tWopt\tIABA\n";
    fileR<<"\tBEB\tABA\tWopt\tIABA\n";
    fileF<<"\tBEB\tABA\tWopt\tIABA\n";
    fileS<<"\tBEB\tABA\tWopt\tIABA\n";
    fileC<<"\tBEB\tABA\tWopt\tIABA\n";
    fileI<<"\tBEB\tABA\tWopt\tIABA\n";
    fileA<<"\tBEB\tABA\tWopt\tIABA\n";
    fileD<<"\tBEB\tABA\tWopt\tIABA\n";
    fileDL<<"\tBEB\tABA\tWopt\tIABA\n";
    filePC<<"\tBEB\tABA\tWopt\tIABA\n";
    for(int index=0; index<26; index++)
    {
        fileT<<nodesT[index]<<"\t";
        fileE<<nodesT[index]<<"\t";
        fileEC<<nodesT[index]<<"\t";
        fileR<<nodesT[index]<<"\t";
        fileF<<nodesT[index]<<"\t";
        fileS<<nodesT[index]<<"\t";
        fileC<<nodesT[index]<<"\t";
        fileI<<nodesT[index]<<"\t";
        fileA<<nodesT[index]<<"\t";
        fileD<<nodesT[index]<<"\t";
        fileDL<<nodesT[index]<<"\t";
        filePC<<nodesT[index]<<"\t";


        for(CSMA_MOD=0; CSMA_MOD<=3; CSMA_MOD++)
        {
            runSimulator(nodesT[index],nodesWopt[index]);
        }
        fileT<<"\n";
        fileE<<"\n";
        fileEC<<"\n";
        fileR<<"\n";
        fileF<<"\n";
        fileS<<"\n";
        fileC<<"\n";
        fileI<<"\n";
        fileA<<"\n";
        fileD<<"\n";
        fileDL<<"\n";
        filePC<<"\n";
    }

    fileT.close();
    fileE.close();
    fileR.close();
    fileF.close();
    fileEC.close();
    fileS.close();
    fileC.close();
    fileI.close();
    fileA.close();
    fileD.close();
    fileDL.close();
    filePC.close();

    system("pause");
    return 0;
}



















